<?php 

include 'connection.php'; 

/* -----------------------------------------------------------------------------------
					function for check login credentials
-------------------------------------------------------------------------------------- */
function reArrayFiles($file_post) {

    $file_ary = array();
    $file_count = count($file_post['name']);
    $file_keys = array_keys($file_post);

    for ($i=0; $i<$file_count; $i++) {
        foreach ($file_keys as $key) {
            $file_ary[$i][$key] = $file_post[$key][$i];
        }
    }

    return $file_ary;
}
function slugify($text)
{
  // replace non letter or digits by -
  $text = preg_replace('~[^\pL\d]+~u', '-', $text);

  // transliterate
  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

  // remove unwanted characters
  $text = preg_replace('~[^-\w]+~', '', $text);

  // trim
  $text = trim($text, '-');

  // remove duplicate -
  $text = preg_replace('~-+~', '-', $text);

  // lowercase
  $text = strtolower($text);

  if (empty($text)) {
    return 'n-a';
  }

  return $text;
}

function get_youtube_id($link){
	$arr = explode('/', $link);
	return $last = array_pop( $arr );
	/*$video_id = explode("?v=", $link); // For videos like http://www.youtube.com/watch?v=...
	if (empty($video_id[1]))
		$video_id = explode("/v/", $link); // For videos like http://www.youtube.com/watch/v/..

	$video_id = explode("&", $video_id[1]); // Deleting any other params
	return $video_id = $video_id[0];*/
}
			function validate_id($id,$pass){
				global $con;
				$sql="SELECT * FROM ak_users WHERE login_id='".$id."' AND login_password='".$pass."'";
				$result=mysqli_query($con,$sql);
				$rowcount=mysqli_num_rows($result);
				if($rowcount)
				{
					return true;
				}
				else{
					return false;
				}
			}		

			function get_all_user(){
				global $con;
				$sql="SELECT * FROM ak_users";
				$result=mysqli_query($con,$sql);
				while($user=mysql_fetch_assoc($result,MYSQL_ASSOC))
				{	
					return($user);
				}
			}
			function get_user($user_id){
				global $con;
				$sql="SELECT * FROM ak_users WHERE login_id='".$user_id."'";
				$result=mysqli_query($con,$sql);
				while($user=mysqli_fetch_assoc($result))
				{	
					return($user);
				}
			}
			function get_images($user_id){
				global $con;
				$sql="SELECT * FROM ak_images WHERE user_id='".$user_id."' ORDER BY created DESC";
				return $result=mysqli_query($con,$sql);
				$full_images=array();

				while($images=mysqli_fetch_array($result,MYSQLI_ASSOC)){
					$full_images[]=$images;	
				}
				return($full_images);
			}
			function get_posts_by_parent($parent_id = 0){
				global $con;
				if($parent_id){
					$sql="SELECT * FROM `ak_posts` WHERE parent_id=".$parent_id;
				}else{
					$sql="SELECT * FROM `ak_posts` WHERE parent_id=0";
				}
				$result=mysqli_query($con,$sql);
				$all_posts=array();

				while($posts=mysqli_fetch_array($result,MYSQLI_ASSOC)){
					$all_posts[]=$posts;	
				}
				return($all_posts);
			}
			function get_post_by_id($post_id = 0){
				global $con;
				if($post_id){
					$sql="SELECT * FROM `ak_posts` WHERE id=".$post_id;
					 $result=mysqli_query($con,$sql);
					 return $result->fetch_assoc();
				}else{
					return false;
				}
			}
			function delete_post_by_id($post_id = 0){
				global $con;
				if($post_id){
					$sql="DELETE FROM `ak_posts` WHERE id=".$post_id." OR parent_id=".$post_id;
					if (mysqli_query($con, $sql)) {
						return true;
					} else {
						return false;
					}
				}
			}
			function insert_folder($title,$description,$image_file,$external_url,$parent_id=0){
				global $con;
				$post_name = slugify($title);
				$sql="INSERT INTO ak_posts(post_title,post_name,post_description,post_image,external_url,post_type,download_file,parent_id,created) VALUES('".$title."','".$post_name."','".$description."','".$image_file."','".$external_url."','folder',NULL,'".$parent_id."',".strtotime('now').")";
				$result=mysqli_query($con,$sql);
				if($result){
					return true;
				}
				else{
					echo "<p class=\"text-danger\">Failed to insert folder</p>";
				}  
			}
			function update_folder($title,$description,$image,$ext_url,$parent_id,$post_id){
				global $con;
				$post_name = slugify($title);

				$sql = "UPDATE `ak_posts` 
				SET 
				`post_title` = '".$title."',
				`post_name` = '".$post_name."',
				`post_description` = '".$description."',
				`post_image` = '".$image."',
				`external_url` = '".$ext_url."',
				`parent_id` = $parent_id
				WHERE `id` = $post_id ";







				/*$sql="INSERT INTO ak_posts(post_title,post_name,post_description,post_image,external_url,post_type,download_file,parent_id,created) VALUES('".$title."','".$post_name."','".$description."','".$image_file."','".$external_url."','folder',NULL,'".$parent_id."',".strtotime('now').")";*/
				$result=mysqli_query($con,$sql);
				if($result){
					return true;
				}
				else{
					echo "<p class=\"text-danger\">Failed to update folder data</p>";
				}  
			}
			function insert_file($title,$description,$image_file,$download_file,$parent_id=0){
				global $con;
				$post_name = slugify($title);
				$sql="INSERT INTO ak_posts(post_title,post_name,post_description,post_image,external_url,post_type,download_file,parent_id,created) VALUES('".$title."','".$post_name."','".$description."','".$image_file."',NULL,'file','".$download_file."','".$parent_id."',".strtotime('now').")";
				$result=mysqli_query($con,$sql);
				if($result){
					return true;
				}
				else{
					echo "<p class=\"text-danger\">Failed to insert file data.</p>";
				}  
			}
			function update_file($title,$description,$image_file,$download_file,$parent_id,$post_id){
				global $con;
				$post_name = slugify($title);
				$sql = "UPDATE `ak_posts` 
				SET 
				`post_title` = '".$title."',
				`post_name` = '".$post_name."',
				`post_description` = '".$description."',
				`post_image` = '".$image_file."',
				`download_file` = '".$download_file."',
				`parent_id` = $parent_id
				WHERE `id` = $post_id ";

				/*$sql="INSERT INTO ak_posts(post_title,post_name,post_description,post_image,external_url,post_type,download_file,parent_id,created) VALUES('".$title."','".$post_name."','".$description."','".$image_file."',NULL,'file','".$download_file."','".$parent_id."',".strtotime('now').")";*/
				$result=mysqli_query($con,$sql);
				if($result){
					return true;
				}
				else{
					echo "<p class=\"text-danger\">Failed to update file data.</p>";
				}  
			}



			function insert_video($title,$description,$video_embed_url,$parent_id=0){
				global $con;
				$post_name = slugify($title);
				 $sql="INSERT INTO ak_posts(post_title,post_name,post_description,post_image,external_url,post_type,download_file,parent_id,video_embed_url,created) VALUES('".$title."','".$post_name."','".$description."',NULL,NULL,'video',NULL,'".$parent_id."','".$video_embed_url."',".strtotime('now').")";
				$result=mysqli_query($con,$sql);
				if($result){
					return true;
				}
				else{
					echo "<p class=\"text-danger\">Failed to insert video data.</p>";
				}  
			}
			function update_video($title,$description,$video_embed_url,$parent_id,$post_id){
				global $con;
				$post_name = slugify($title);

				$sql = "UPDATE `ak_posts` 
				SET 
				`post_title` = '".$title."',
				`post_name` = '".$post_name."',
				`post_description` = '".$description."',
				`video_embed_url` = '".$video_embed_url."',
				`parent_id` = $parent_id
				WHERE `id` = $post_id ";

				 /*$sql="INSERT INTO ak_posts(post_title,post_name,post_description,post_image,external_url,post_type,download_file,parent_id,video_embed_url,created) VALUES('".$title."','".$post_name."','".$description."',NULL,NULL,'video',NULL,'".$parent_id."','".$video_embed_url."',".strtotime('now').")";*/
				$result=mysqli_query($con,$sql);
				if($result){
					return true;
				}
				else{
					echo "<p class=\"text-danger\">Failed to update video data.</p>";
				}  
			}



			function insert_image($image,$user){
				global $con;
				$sql="INSERT INTO ak_images(img_name,user_id,created) VALUES('".$image."','".$user."',".strtotime('now').")";
				$result=mysqli_query($con,$sql);
				if($result){
					return true;
				}
				else{
					echo "<p class=\"text-danger\">Failed to insert Image</p>";
				}  
			}
			function update_user_password($newpassword){
				global $con;
				$sql="UPDATE ak_users SET login_password='".$newpassword."'";
				$result=mysqli_query($con,$sql);
				if($result){
					return true;
				}
				else{
					echo "<p class=\"text-danger\">Failed to insert Image</p>";
				}  
			}



function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}
			
/* -----------------------------------------------------------------------------------
-------------------------------------------------------------------------------------- */




	/*
	function for fetching data with join all tables in database.it also returns total no. of rows count with passing parameter 'count'.
	it accepts either "offset value" or "offset+('ASC' or 'DESC')+<column name>" or " 'count' "
	*/
	
	
/* function fetch_data($off,$asc="",$col=""){
	$c="";
	if($asc && $col){
		$asc="ORDER BY ".$col." ".$asc;
	}else if(!is_numeric($off) 
			&& $off=='count'){
				$c=$off;
				$off="";
	}
	if(is_numeric($off)){
		$off="LIMIT 5 OFFSET ".$off;
	}
	$sql="SELECT product.product_id,product.product_name,product.product_color,product.product_size,product.product_price,categories.category_name,brands.brand_name FROM product LEFT JOIN categories ON product.fk_product_category_id=categories.category_id LEFT JOIN brands ON product.fk_product_brand_id=brands.brand_id $asc $off";
	$result=mysqli_query($sql);
	if($c=='count'){
		return(mysql_num_rows($result));
	}else{
		return $result;
	}
	
	mysql_close($con);
}

 */
	/**********************
	function for fetching data by product_id on the product edit page for editing/updating.
	it accepts product_id as argument.
	****************************/
/* function fetch_edit_data($id){
	$sql="SELECT * FROM product WHERE product_id='$id'";
	$result=mysqli_query($sql);
	while($row=mysql_fetch_assoc($result,MYSQL_ASSOC)){
		return $row; 
	}
	mysql_close($con);
}
 */

	/**********************
	function for "update data" on edit product page with update click.
	****************************/
/* function update_data($id,$name,$color,$size,$price,$brand,$category){
	$sql="UPDATE product SET product_name='$name',product_color='$color',product_size='$size',product_price=$price,fk_product_brand_id='$brand',fk_product_category_id='$category' WHERE product_id='$id' ";
	$result=mysqli_query($sql)or die ('Error updating database: '.mysql_error());
	if($result){
		echo "<p class=\"text-success\">Data Inserted Successfully</p>";
		header("LOCATION: Product.php");
	}
	else{
		echo "<p class=\"text-danger\">Failed to insert Data</p>";
	}
	mysql_close($con);
} */



	/**********************
	function for insert new product data into database.
	****************************/
/* function insert_data($name,$color,$size,$price,$brand,$category){
	$sql="INSERT INTO product(product_name,product_color,product_size,product_price,fk_product_brand_id,fk_product_category_id) VALUES('$name','$color','$size',$price,$brand,$category)";
	$result=mysqli_query($sql)or die ('Error updating database: '.mysql_error());
 
	 if($result){
		echo "<p class=\"text-success\">Data Inserted Successfully</p>";
		header("LOCATION: Product.php");
	}
	else{
		echo "<p class=\"text-danger\">Failed to insert Data</p>";
	}  
	mysql_close($con);
} */



	/**********************
	function for search product by input searching keywords.
	it accepts searching keyword as argument.
	****************************/
/* function search_product($key){
	$sql="SELECT product.product_id,product.product_name,product.product_color,product.product_size,product.product_price,categories.category_name,brands.brand_name FROM product LEFT JOIN categories ON product.fk_product_category_id=categories.category_id LEFT JOIN brands ON product.fk_product_brand_id=brands.brand_id
	WHERE product_name LIKE '%$key%' OR product_color LIKE '%$key%' OR product_size LIKE '%$key%' OR product_price LIKE '%$key%' OR categories.category_name LIKE '%$key%' OR brands.brand_name LIKE '%$key%'";
	$result=mysqli_query($sql);
	return $result;
	mysql_close($con);
}
 */

	/**********************
	function for delete product by product_id from table.
	it accepts product_id as argument.
	****************************/
/* function delete_data($d_id){
	$sql="DELETE FROM product WHERE product_id='$d_id'";
	$result=mysqli_query($sql);
	if($result){
		echo "<p class=\"text-success\">Deleted Successfully</p>";
		header("LOCATION: Product.php");
	}
	else{
		echo "<p class=\"text-danger\">Failed to delete Data</p>";
	}
	mysql_close($con);
} */

	/**********************
	function for fetching all data from brand table.
	****************************/
/* function get_brand(){
	$sql="SELECT * FROM brands";
	$result=mysqli_query($sql);
	return $result;
	mysql_close($con);
} */



	/**********************
	function for fetching all data from categories table.
	****************************/
/* function get_category(){
	$sql="SELECT * FROM categories";
	$result=mysqli_query($sql);
	return $result;
	mysql_close($con);
} */

?>